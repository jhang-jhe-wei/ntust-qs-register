class CreateRecommenders < ActiveRecord::Migration[6.1]
  def change
    create_table :recommenders do |t|
      t.string :title
      t.string :first_name
      t.string :last_name
      t.string :job_title
      t.string :department
      t.references :institution, null: true, foreign_key: true
      t.references :industry, null: true, foreign_key: true

      t.timestamps
    end
  end
end
