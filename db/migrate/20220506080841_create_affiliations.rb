class CreateAffiliations < ActiveRecord::Migration[6.1]
  def change
    create_table :affiliations, id: false do |t|
      t.bigint :id, options: 'PRIMARY KEY'
      t.string :title
      t.string :city
      t.references :country, null: false, foreign_key: true
      t.integer :document_count

      t.timestamps
    end
    add_index :affiliations, :id
  end
end
